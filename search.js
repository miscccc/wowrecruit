
function get_params(){
    var params = {};
    var req_maxhits = document.getElementById("maxhits");
    var req_faction = document.getElementById("faction");
    var req_raids_week = document.getElementById("raids");
    var req_lang = document.getElementById("language");
    var reqclass = document.getElementById("targetclass");
    var req_region = document.getElementById("reqregion");
    var req_difficulty = document.getElementById("difficulty");
    var req_raid = document.getElementById("raid");
    var req_partition = document.getElementById("partition");
    var req_exp_perf = document.getElementById("exp_perf");
    return {
        max_hits : req_maxhits.options[req_maxhits.selectedIndex].value,
        faction : req_faction.options[req_faction.selectedIndex].value,
        raids_week : req_raids_week.options[req_raids_week.selectedIndex].value,
        lang : req_lang.options[req_lang.selectedIndex].value,
        req_class : reqclass.options[reqclass.selectedIndex].value,
        limit : document.getElementById("ilvlreq").value,
        region : req_region.options[req_region.selectedIndex].value,
        exp_perf : req_exp_perf.options[req_exp_perf.selectedIndex].value,
        raid : req_raid.options[req_raid.selectedIndex].value,
        difficulty : req_difficulty.options[req_difficulty.selectedIndex].value,
        partition : req_partition.options[req_partition.selectedIndex].value
    }
}

function get(url, params, providedFunction){
    var xhttp = new XMLHttpRequest();
    var post_attrs = "?";
    for (var key in params) {
        post_attrs += key + "=" + params[key] + "&";
    }
    console.log(url, params);
    xhttp.open("GET",url + post_attrs, true);
    xhttp.onreadystatechange = function(){
        if(xhttp.readyState === 4){
            console.log("Responsetext: ", xhttp.responseText);
            var response = JSON.parse(xhttp.responseText);
            response.status = xhttp.status;
            
            providedFunction(response);
        }
    }
    xhttp.setRequestHeader("Content-type", 
                           "application/x-www-form-urlencoded");
    xhttp.send();
}


function get_search(){
    destroy_old_results()
    display_loading();
    get("/search", get_params(), function(response){
        build_players(response);
        console.log(response);
    });
}

function get_image(faction){
    if (faction == "Horde"){
        return "/static/images/horde.jpg";
    } else {
        return "/static/images/alliance.jpg";
    }
}

function display_loading(){
    document.getElementById("container").style.setProperty("display", "none");
    document.getElementById("loadingscreen").style.setProperty("display", "block");

}

function destroy_old_results(){

    var element = document.getElementById("container");
    element.parentNode.removeChild(element);
    var col = document.createElement("div");
    col.className = "col";
    col.id = "container";

    var row = document.getElementById("containerrow");
    row.appendChild(col);

}

function build_players(playerparams){
    document.getElementById("container").style.setProperty("display", "block");
    document.getElementById("loadingscreen").style.setProperty("display", "none");
    for(var key in playerparams){
        if(playerparams.hasOwnProperty(key)){
            var vallist = key.split(" ");
            
            if(vallist[0] == "status"){
                break;
            }

            var pname = vallist[0];
            var pfaction = vallist[1];
            var pclass = vallist[2];
            if (vallist.length == 5){
                var prealm = vallist[3];
                var pilvl = vallist[4];
            } else {
                var prealm = vallist[3] + "-" + vallist[4];
                var pilvl = vallist[5];
            }
            var pspec = "";

            var wowlink = playerparams[key];
            var link = "http://www.warcraftlogs.com/character/eu/";
            var loglink = link + prealm + "/" + pname;

        }
    
        var player = document.createElement("div");
        player.id = "searchcontainer";
        
        var row1 = document.createElement("div");
        row1.className = "row";
        row1.id = "scrow";
        player.appendChild(row1);

        var col1 = document.createElement("div");
        col1.className = "col-sm-3";
        col1.id = "space2";

        var img = document.createElement("img");
        img.src = get_image(pfaction);
        img.id = "playerpic";
        
        col1.appendChild(img);

        var col2 = document.createElement("div");
        col2.className = "col-sm-6";
        col2.id = "space";

        var playername = document.createElement("h2");
        playername.id = "playername";
        playername.textContent = pname;
        
        var server = document.createElement("h4");
        server.id = "server";
        server.textContent = prealm;
        
        col2.appendChild(playername);
        col2.appendChild(server);    
        
        var col3 = document.createElement("div");
        col3.className = "col-sm-3";
        col3.id = "space";
        
        var ilvl = document.createElement("h4");
        ilvl.id = "ilvl";
        ilvl.textContent = pilvl;

        var _class = document.createElement("h4");
        _class.id = "class";
        _class.textContent = pclass;

        var spec = document.createElement("h4");
        spec.id = "spec";
        spec.textContent = pspec;

        col3.appendChild(ilvl);
        col3.appendChild(_class);
        col3.appendChild(spec);

        row1.appendChild(col1);
        row1.appendChild(col2);
        row1.appendChild(col3);
        
        var row2 = document.createElement("div");
        row2.className = "row";
        row2.id = "scrow2";
        player.appendChild(row2);

        var col1 = document.createElement("div");
        col1.className = "col-sm-6";
        col1.id = "space";

        var col2 = document.createElement("div");
        col2.className = "col-sm-6";
        col2.id = "space";
        
        var center1 = document.createElement("center");

        var wowplink = document.createElement("a");
        wowplink.href = wowlink;
        wowplink.target = "_blank";
        wowplink.innerHTML = "Wowprogress";
        wowplink.id = "Wowprogress";
        
        center1.appendChild(wowplink);
        col1.appendChild(center1);

        var center2 = document.createElement("center");

        var warlink = document.createElement("a");
        warlink.href = loglink;
        warlink.target = "_blank";
        warlink.innerHTML = "Warcraftlogs";
        warlink.id = "warcraftlogs";

        center2.appendChild(warlink);
        col2.appendChild(center2);

        row2.appendChild(col1);
        row2.appendChild(col2);


        document.getElementById("container").appendChild(player);
    }
    document.body.style.setProperty("height", document.body.scrollHeight + "px");
    
    
}
