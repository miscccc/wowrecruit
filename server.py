from flask import Flask, request, send_file
import fetch_users
import json
app = Flask(__name__)


@app.route("/search")
def search(): 
    try:
        params = {key: request.args[key] for key in request.args.keys()}
        result = fetch_users.main(params)
        print("RESULT= ", result)
        return json.dumps(result)
    except Exception as e:
        return json.dumps({"Error": str(e)}), 500

@app.route("/<path>.css")
def serve_css(path):
    return send_file(path + ".css")

@app.route("/<path>.js")
def serve_js(path):
    return send_file(path + ".js")

@app.route("/<path>.jpg")
def serve_jpg(path):
    return send_file(path + ".jpg")

@app.route("/")
def home():
    return send_file("index.html")

if __name__ == "__main__":
    app.run(host="0.0.0.0",debug=True)

