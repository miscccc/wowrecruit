from sys import argv
from selenium import webdriver
import requests
import json

def passes_criteria(name, realm, limit, req_faction, req_region, ilvl, race, region, _class, req_class, exp_perf, difficulty, raid, partition):
    req_class_list = req_class.split(",")
    if req_region != "" and req_region != region:
        return False
    if req_faction != "" and get_faction(race) != req_faction:
        return False
    if ilvl < float(limit):
        return False
    if req_class != "" and _class not in req_class_list:
        return False
    if int(exp_perf) != 0 and not check_average_performance(name, realm, region, exp_perf, difficulty, raid, partition):
        return False
    return True


def get_faction(race):
    if race in ["human", "gnome", "dwarf", "night elf", "draenei", "worgen"]:
        faction = "Alliance"
                
    else:
        faction = "Horde"
    return faction
def find_users_on_page(table, criteria_method):
    result = {}
    print("Start searching")
    for row in table.find_elements_by_tag_name("tr")[1:]:
        try:
            character = row.find_element_by_css_selector("a.character")
            race, _class = character.get_attribute("data-hint").rsplit(" ", 1)
            ilvl = float(row.find_elements_by_class_name("center")[0].text)
            charregion, realm = row.find_element_by_css_selector("a.realm").text.split("-")
            name = character.text
            if criteria_method(name, realm, race, _class, ilvl, charregion):
                faction = get_faction(race)
                print(name)
                result["{} {} {} {} {}".format(name, faction, _class, realm, ilvl)] = character.get_attribute("href")
        except Exception as e:
            print(e)
            raise
    return result


def below_limit(limit, table):
    return float(table.find_elements_by_tag_name("tr")[-1].find_elements_by_class_name("center")[0].text) < float(limit)


def get_user_list(br, raids_week="", lang="", limit=0, faction="", region="", req_class="", max_hits=1, exp_perf=0, difficulty="", raid="", partition=2):
    def criteria_method(name, realm, race, _class, ilvl, charregion):
        return passes_criteria(name, realm, limit, faction, region, ilvl, race, charregion, _class, req_class, exp_perf, difficulty, raid, partition)
    br.get("https://www.wowprogress.com/gearscore/?lfg=1&raids_week={}&lang={}"
            .format(raids_week, lang))
    result = {}
    click = 0
    while click < int(max_hits):
        table = br.find_elements_by_class_name("rating")[0]
        result.update(find_users_on_page(table, criteria_method))
        #if len(result) >= max_hits:
        #    print("Färdiga!")
        #    break;
        table = br.find_elements_by_class_name("rating")[0]
        if (below_limit(limit, table) and 
            not br.find_elements_by_css_selector("span.navNext.disabled")
           ):
            break
        click += 1
        br.find_elements_by_class_name("navNext")[0].click()
    return result


def check_average_performance(name, realm, region, exp_perf, difficulty, raid, partition):
    realmlist = realm.split(" ")
    if len(realmlist) == 2:
        realm = realmlist[0] + "-" + realmlist[1]
    else:
        realm = realmlist[0]

    metric="dps"
    response = requests.get(
    "https://www.warcraftlogs.com/v1/parses/character/{}/{}/{}?partition={}&metric={}&api_key=94b6da31565d96571c2cf2a55bc3989a"
            .format(name, realm, region, partition, metric))
    difficulties = {'Mythic' : 5, 'Heroic' : 4, 'Normal' : 3 }
    raids = {
            'Nighthold' : 
            ["Skorpyron", "Chronomatic Anomaly", 
                "Trilliax", "Spellblade Aluriel", 
                "Krosus", "Star Augur Etraeus", 
                "High Botanist Tel'arn", "Grand Magistrix Elisande", 
                "Tichondrius", "Gul'Dan"], 
            'Trial of Valor' : ["Odyn", "Guarm", "Helya"], 
            'Emerald nightmare' : []}
    if not response.text == "[]":
        responseobj = json.loads(response.text)
        avg_perf = 0
        avg_list = {}
        spec_list = {}
        if type(responseobj) is list:
            for boss in responseobj:
                if boss == "error":
                    continue
                if boss["name"] in raids[raid]:
                    if boss["difficulty"] == difficulties[difficulty] or difficulty == "":
                        for specs in boss["specs"]:
                            if specs["spec"] not in spec_list:
                                spec_list[specs["spec"]] = 0
                            if boss["name"] not in avg_list:
                                avg_list[boss["name"]] = 0
                            avg_val = specs["best_historical_percent"]
                            if avg_list[boss["name"]] < avg_val:
                                avg_list[boss["name"]] = avg_val
                        spec_list[specs["spec"]] = get_avg_val(avg_list)
        if metric == "hps":
            for spec in spec_list:
                for current_key in ["Restoration", "Holy", "Discipline"]:
                    if spec == current_key:
                        if spec_list[current_key] >= float(exp_perf):
                            return True
                        return False
        else: 
            other_specs = [key for key in spec_list if key not in ["Restoration", "Holy", "Discipline"]]
            for spec in spec_list:
                for current_key in other_specs:
                    if spec == current_key:
                        if spec_list[current_key] >= float(exp_perf):
                            return True
                        return False
        return False
    print("Något gick fel")
    return True

def get_avg_val(avg_list):
    avg_perf = 0
    if avg_list != {}:
        for value in avg_list:
            avg_perf += avg_list[value]
        avg_perf = avg_perf/len(avg_list)
        return avg_perf
    return 0
def get_user_info(character):
    br.get(character)


def get_args():
    args = {}
    for arg in argv[1:]:
        argname, argval = arg.split("=")
        args[argname] = argval
    return args

def main(args):
    br = webdriver.PhantomJS("./phantomjs/bin/phantomjs", service_log_path='/dev/null')
    return get_user_list(br, **args)


if __name__ == "__main__":
    args = get_args()
    user_list = main(args)
    for key, val in user_list.items():
        
        print("{}: {}".format(key, val))
